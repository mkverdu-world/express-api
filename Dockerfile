# Version 4
FROM node:18.16.0-alpine3.18 AS builder

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY . .

RUN npm install \
&& npm run build

FROM node:18.16.0-alpine3.18 AS final

# Create app directory
WORKDIR /usr/src/app

# Bundle dist source
COPY --from=builder /usr/src/app/dist ./dist
COPY package*.json .

# ENVs
ENV PORT=8080
ENV NODE_ENV=docker

RUN npm install --production

EXPOSE $PORT
CMD [ "npm", "run", "start:dev-for-docker" ]