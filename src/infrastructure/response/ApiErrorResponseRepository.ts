import { ApiErrorResponse } from "../../domain/dao/response/ApiErrorResponse";
import ErrorResponseRepository from "../../domain/repository/ErrorResponseRepository";

export class ApiErrorResponseRepository implements ErrorResponseRepository {
  async getErrorResponse(
    response: ApiErrorResponse
  ): Promise<ApiErrorResponse> {
    return Promise.reject(
      new ApiErrorResponse(
        response.code,
        response.httpCode,
        "",
        response.isOperational,
        response.payload,
        undefined,
        undefined
      )
    );
  }
}
