import  { UserApplication } from "../../application/user/UserApplication";
import Config from "../../config";
import UserRepository from "../../domain/repository/UserReporitory";
import { UsersController } from "../../router/v1/users/UsersController";
import { ApiResponseUtil } from "../../utils/ApiResponseUtil";
import { SqliteUserRepository } from "./repository/SqliteUserRepository"

const config = Config.getInstance();

const repositories: Record<
  string,
  {
    new (): UserRepository;
  }
> = {
  sqlite: SqliteUserRepository,
};

const getUserRepository = () => {
    if (repositories[config.databaseType]) {
      return new repositories[config.databaseType]();
    } else {
      throw Error(`Invalid Database type '${config.databaseType}'`);
    }
  };
  
  const responseUtil = new ApiResponseUtil();
  const userApplication = new UserApplication(getUserRepository());
  
  export const userController = new UsersController(
    userApplication,
    responseUtil
  );
