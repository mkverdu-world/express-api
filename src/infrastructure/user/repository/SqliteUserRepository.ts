import { AsyncDatabase } from "promised-sqlite3";

import Sqlite from "../../../db/sqlite";
import UserDAO from "../../../domain/dao/UserDAO";
import ApiErrorNames from "../../../domain/dao/response/ApiErrorNames";
import { ApiErrorResponse } from "../../../domain/dao/response/ApiErrorResponse";
import HttpStatusCodes from "../../../domain/dao/response/HttpStatusCodes";
import ErrorResponseRepository from "../../../domain/repository/ErrorResponseRepository";
import UserReporitory from "../../../domain/repository/UserReporitory";

export class SqliteUserRepository implements UserReporitory {

    private static sqlFindAll = "select rowId AS id, alias, '***' AS password from users";
    private static sqlFindById = "select rowId AS id, alias, '***' AS password from users where rowId = ?";
    private static sqlValidate = "select rowId AS id, alias, '***' AS password from users where alias = ? AND password = ?";

    private static async getSqliteDB(): Promise<AsyncDatabase> {
        const sqlite = await Sqlite.getInstance();
        return sqlite.db;
    }
    
    async findAll(): Promise<UserDAO[]> {
        const params: String[] = [];
        let response: UserDAO[] = [];

        const sqliteDB = await SqliteUserRepository.getSqliteDB();

        response = await sqliteDB.all<UserDAO>(
            SqliteUserRepository.sqlFindAll,
            params
        );

        return response;
    }
    
    async findOne(_item: Partial<UserDAO>): Promise<UserDAO> {
        let query: string | undefined;
        let params: string[] | undefined;
        let response:UserDAO | undefined;

        if (_item.alias && _item.password){
            query = SqliteUserRepository.sqlValidate
            params = [_item.alias, _item.password]
        }else if (_item.id){
            query = SqliteUserRepository.sqlFindById
            params = [_item.id.toString()]
        }

        if (query && params){
            const sqliteDB = await SqliteUserRepository.getSqliteDB();

            response = await sqliteDB.get<UserDAO>(query, params);
        }

        if (!response) {
            return Promise.reject(
                new ApiErrorResponse(
                    ApiErrorNames.USER_NOT_FOUND,
                    HttpStatusCodes.NOT_FOUND,
                    "User not found in DDBB",
                    true,
                    {..._item},
                )
            );
        }

        return response;
    }

}