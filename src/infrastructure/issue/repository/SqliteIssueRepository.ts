import { AsyncDatabase } from "promised-sqlite3";

import Sqlite from "../../../db/sqlite";
import IssueDAO from "../../../domain/dao/IssueDAO";
import ApiErrorNames from "../../../domain/dao/response/ApiErrorNames";
import { ApiErrorResponse } from "../../../domain/dao/response/ApiErrorResponse";
import HttpStatusCodes from "../../../domain/dao/response/HttpStatusCodes";
import ErrorResponseRepository from "../../../domain/repository/ErrorResponseRepository";
import IssueRepository from "../../../domain/repository/IssueRepository";

export class SqliteIssueRepository implements IssueRepository {

  private static sqlFindAll =
    "select rowId AS id, title, description from issues";
  private static sqlFindOne =
    "select rowId AS id, title, description from issues where rowId = ?";

  private static async getSqliteDB(): Promise<AsyncDatabase> {
    const sqlite = await Sqlite.getInstance();
    return sqlite.db;
  }

  async findAll(): Promise<IssueDAO[]> {
    const params: string[] = [];
    let response: IssueDAO[] = [];

    const sqliteDB = await SqliteIssueRepository.getSqliteDB();

    response = await sqliteDB.all<IssueDAO>(
      SqliteIssueRepository.sqlFindAll,
      params
    );

    return response;
  }

  async findOne(
    _id: string | Partial<IssueDAO>
  ): Promise<IssueDAO> {
    const params: string[] = [_id.toString()];

    const sqliteDB = await SqliteIssueRepository.getSqliteDB();

    const response: IssueDAO = await sqliteDB.get<IssueDAO>(
      SqliteIssueRepository.sqlFindOne,
      params
    );

    if (!response) {
      return Promise.reject(
        new ApiErrorResponse(
          ApiErrorNames.ISSUE_NOT_FOUND,
          HttpStatusCodes.NOT_FOUND,
          "Issue not found in DDBB",
          true,
          {
            id: _id,
          }
        )
      );
    }

    return response;
  }
}
