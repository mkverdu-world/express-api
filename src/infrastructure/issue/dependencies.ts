import { IssueApplication } from "../../application/issues/IssueApplication";
import Config from "../../config";
import IssueRepository from "../../domain/repository/IssueRepository";
import { IssuesController } from "../../router/v1/issues/IssuesController";
import { ApiResponseUtil } from "../../utils/ApiResponseUtil";
import { ApiErrorResponseRepository } from "../response/ApiErrorResponseRepository";
import { SqliteIssueRepository } from "./repository/SqliteIssueRepository";

const config = Config.getInstance();

const repositories: Record<
  string,
  {
    new (): IssueRepository;
  }
> = {
  sqlite: SqliteIssueRepository,
};

const apiErrorResponseRepository = new ApiErrorResponseRepository();

const getIssueRepository = () => {
  if (repositories[config.databaseType]) {
    return new repositories[config.databaseType]();
  } else {
    throw Error(`Invalid Database type '${config.databaseType}'`);
  }
};

const responseUtil = new ApiResponseUtil();
const issueApplication = new IssueApplication(getIssueRepository());

export const issueController = new IssuesController(
  issueApplication,
  responseUtil
);
