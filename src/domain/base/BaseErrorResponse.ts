interface Reader<T> {
  getErrorResponse(response: T): Promise<T>;
}
type BaseErrorResponse<T> = Reader<T>;

export default BaseErrorResponse;
