export default class UserDAO {
    public readonly id: number;
    public readonly alias: string;
    public readonly password: string;
}