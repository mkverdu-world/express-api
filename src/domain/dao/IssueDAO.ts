export default class IssueDAO {
  public readonly id: number;
  public readonly title: string;
  public readonly description: string;
}
