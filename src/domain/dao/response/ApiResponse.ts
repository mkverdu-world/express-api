import { ApiErrorResponse } from "./ApiErrorResponse";

export class ApiResponse<TypeResponseData = null> {
  private readonly _isApiResponse: boolean;
  readonly statusCode: number = -1;
  readonly success: boolean;
  readonly message: string = "";
  readonly data?: TypeResponseData | null;
  readonly payload?: object;
  readonly error?: ApiErrorResponse;

  constructor({
    statusCode,
    success,
    message,
    data,
    payload,
    error,
  }: Omit<ApiResponse<TypeResponseData>, "isApiResponse">) {
    this._isApiResponse = true;
    this.statusCode = statusCode;
    this.success = success;
    this.message = message;
    this.data = data;
    this.payload = payload;
    this.error = error;
  }

  get isApiResponse() {
    return this._isApiResponse;
  }
}
