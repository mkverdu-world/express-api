import crypto from "crypto";

import ApiErrorNames from "./ApiErrorNames";
import HttpStatusCodes from "./HttpStatusCodes";

export class ApiErrorResponse extends Error {
  public tagId: string;
  public readonly name: string;
  public readonly code: ApiErrorNames | undefined;
  public readonly httpName?: string;
  public readonly httpCode: HttpStatusCodes;
  public readonly isOperational: boolean;
  public readonly payload: object | undefined;
  public readonly data?: object | undefined;
  public readonly stackTrace?: string | undefined;
  readonly [key: string]: any;

  constructor(
    code: ApiErrorNames | undefined,
    httpCode: HttpStatusCodes,
    description: string,
    isOperational: boolean,
    payload: object | undefined,
    data?: object,
    previousStack?: string // Example for use the property '(e as Error)['stack']'
  ) {
    super(description);

    Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain

    this.tagId = crypto.randomBytes(20).toString("hex");
    this.code = code;
    this.name = code ? ApiErrorNames[code] : "";
    this.httpCode = httpCode;
    this.httpName = HttpStatusCodes[httpCode];
    this.isOperational = isOperational;
    this.payload = payload;
    this.data = data;

    Error.captureStackTrace(this, this.constructor);
    if (previousStack) {
      this.stack += "\nFrom previous " + previousStack;
    }
    this.stackTrace = this.stack;
  }
}
