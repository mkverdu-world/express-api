enum ApiErrorNames {
  // ISSUES
  ISSUE_NOT_FOUND = 101,
  // TOKEN
  TOKEN_NOT_PRESENT = 201,
  TOKEN_INVALID = 202,
  // USERS
  USER_NOT_FOUND = 101,
}

export default ApiErrorNames;
