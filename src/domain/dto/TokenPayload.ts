import { JwtPayload } from "jsonwebtoken";

import UserTokenData from "./UserTokenData";

export default interface TokenPayload extends JwtPayload {
  data?: UserTokenData;
}
