export default interface UserTokenData {
  alias: string;
  rights: string[];
}
