import UserDAO from "../../dao/UserDAO";

export default class UserValidateResponseDTO {
    public readonly token: string;
    public readonly user: UserDAO;
}