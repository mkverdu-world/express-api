import UserDAO from "../../dao/UserDAO";

export default class UserValidateRequestDTO {
    public readonly alias: string;
    public readonly password: string;
}