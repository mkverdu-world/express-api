import BaseRepository from "../base/BaseRepository";
import UserDAO from "../dao/UserDAO";
import { ApiErrorResponse } from "../dao/response/ApiErrorResponse";
import UserValidateRequestDTO from "../dto/users/UserValidateRequestDTO";
import UserValidateResponseDTO from "../dto/users/UserValidateResponseDTO";

export default abstract class UserRepository implements Pick<BaseRepository<UserDAO>, "findAll" | "findOne"> {
    async findAll(): Promise<UserDAO[]> {
        throw new Error("Method not implemented.");
    }
    async findOne(item:Partial<UserDAO>): Promise<UserDAO> {
        throw new Error("Method not implemented.");
    }
    
}