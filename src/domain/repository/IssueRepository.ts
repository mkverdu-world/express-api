import BaseRepository from "../base/BaseRepository";
import IssueDAO from "../dao/IssueDAO";
import { ApiErrorResponse } from "../dao/response/ApiErrorResponse";

export default abstract class IssueRepository
  implements
    Pick<BaseRepository<IssueDAO>, "findAll" | "findOne">
{
  async findAll(): Promise<IssueDAO[]> {
    return Promise.reject("Method not implemented.");
  }

  async findOne(
    id: string | Partial<IssueDAO>
  ): Promise<IssueDAO> {
    return Promise.reject("Method not implemented.");
  }
}
