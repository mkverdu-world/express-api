import BaseErrorResponse from "../base/BaseErrorResponse";
import { ApiErrorResponse } from "../dao/response/ApiErrorResponse";

export default abstract class ErrorResponseRepository
  implements BaseErrorResponse<ApiErrorResponse>
{
  async getErrorResponse(
    response: ApiErrorResponse
  ): Promise<ApiErrorResponse> {
    return Promise.reject("Method not implemented.");
  }
}
