import jsonwebtoken, { JwtPayload } from "jsonwebtoken";

import Config from "../config/index";
import TokenPayload from "../domain/dto/TokenPayload";
import UserTokenData from "../domain/dto/UserTokenData";

export default class TokenUtils {
  private static config = Config.getInstance();

  static sing = (_playerValidation: UserTokenData): string => {
    const incrementExpiration = this.config.jwtExpirationMinutes * 60000;
    const expiration: number = new Date().getTime() + incrementExpiration;
    const payload: TokenPayload = {
      sub: _playerValidation.alias,
      iat: new Date().getTime(),
      exp: expiration,
      data: _playerValidation,
    };
    return jsonwebtoken.sign(payload, this.config.jwtSecret);
  };

  static verify = async (_token: string): Promise<string | JwtPayload> => {
    return jsonwebtoken.verify(_token, this.config.jwtSecret, {
      complete: false,
    });
  };
}
