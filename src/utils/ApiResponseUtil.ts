import { ApiErrorResponse } from "../domain/dao/response/ApiErrorResponse";
import { ApiResponse } from "../domain/dao/response/ApiResponse";
import HttpStatusCodes from "../domain/dao/response/HttpStatusCodes";

export class ApiResponseUtil {
  customSuccess<TypeResponseData>(
    _statusCode: number,
    _message: string,
    _data?: TypeResponseData,
    _payload?: object
  ): ApiResponse<TypeResponseData> {
    const response: Omit<ApiResponse<TypeResponseData>, "isApiResponse"> = {
      statusCode: _statusCode,
      success: true,
      message: _message,
      data: _data,
      payload: _payload,
    };
    return new ApiResponse(response);
  }

  customFailure<TypeResponseData>(
    _statusCode: number,
    _message: string,
    _data?: TypeResponseData,
    _payload?: object,
    _error?: ApiErrorResponse
  ): ApiResponse<TypeResponseData> {
    const response: Omit<ApiResponse<TypeResponseData>, "isApiResponse"> = {
      statusCode: _statusCode,
      success: false,
      message: _message,
      data: _data,
      payload: _payload,
      error: _error,
    };
    return new ApiResponse(response);
  }

  unauthorized = (_payload: object) => {
    const response: Omit<ApiResponse<null>, "isApiResponse"> = {
      statusCode: HttpStatusCodes.UNAUTHORIZED,
      success: false,
      message: HttpStatusCodes[HttpStatusCodes.UNAUTHORIZED],
      payload: _payload,
      error: undefined,
    };
    return new ApiResponse(response);
  };

  notFound(_payload: object) {
    const response: Omit<ApiResponse<null>, "isApiResponse"> = {
      statusCode: HttpStatusCodes.NOT_FOUND,
      success: false,
      message: HttpStatusCodes[HttpStatusCodes.NOT_FOUND],
      payload: _payload,
      error: undefined,
    };
    return new ApiResponse(response);
  }

  success<TypeResponseData>(
    _data?: TypeResponseData,
    _payload?: object
  ): ApiResponse<TypeResponseData> {
    const response: Omit<ApiResponse<TypeResponseData>, "isApiResponse"> = {
      statusCode: HttpStatusCodes.OK,
      success: true,
      message: HttpStatusCodes[HttpStatusCodes.OK],
      data: _data,
      payload: _payload,
    };
    return new ApiResponse(response);
  }

  created<TypeResponseData>(
    _data?: TypeResponseData,
    _payload?: object
  ): ApiResponse<TypeResponseData> {
    const response: Omit<ApiResponse<TypeResponseData>, "isApiResponse"> = {
      statusCode: HttpStatusCodes.CREATED,
      success: true,
      message: HttpStatusCodes[HttpStatusCodes.CREATED],
      data: _data,
      payload: _payload,
    };
    return new ApiResponse(response);
  }

  internalError<TypeResponseData>(
    _data?: TypeResponseData,
    _error?: ApiErrorResponse,
    _payload?: object
  ): ApiResponse<TypeResponseData> {
    const response: Omit<ApiResponse<TypeResponseData>, "isApiResponse"> = {
      statusCode: HttpStatusCodes.INTERNAL_SERVER_ERROR,
      success: false,
      message: HttpStatusCodes[HttpStatusCodes.INTERNAL_SERVER_ERROR],
      data: _data,
      payload: _payload,
      error: _error,
    };
    return new ApiResponse(response);
  }

  badRequest(_error?: ApiErrorResponse, _payload?: object) {
    const response: Omit<ApiResponse<null>, "isApiResponse"> = {
      statusCode: HttpStatusCodes.BAD_REQUEST,
      success: false,
      message: HttpStatusCodes[HttpStatusCodes.BAD_REQUEST],
      payload: _payload,
      error: _error,
    };
    return new ApiResponse(response);
  }
}
