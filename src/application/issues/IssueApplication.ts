import IssueDAO from "../../domain/dao/IssueDAO";
import IssueRepository from "../../domain/repository/IssueRepository";

export class IssueApplication {
  constructor(private readonly issueRepository: IssueRepository) {}

  async findAll(): Promise<IssueDAO[]> {
    return await this.issueRepository.findAll();
  }

  async findOne(_id: number): Promise<IssueDAO> {
    return await this.issueRepository.findOne(_id.toString());
  }
}
