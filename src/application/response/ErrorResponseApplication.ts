import { ApiErrorResponse } from "../../domain/dao/response/ApiErrorResponse";
import ErrorResponseRepository from "../../domain/repository/ErrorResponseRepository";

export class ErrorResponseApplication {
  constructor(
    private readonly errorResponseRepository: ErrorResponseRepository
  ) {}

  async getErrorResponse(
    response: ApiErrorResponse
  ): Promise<ApiErrorResponse> {
    return await this.errorResponseRepository.getErrorResponse(response);
  }
}
