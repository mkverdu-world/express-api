import UserDAO from "../../domain/dao/UserDAO";
import UserValidateRequestDTO from "../../domain/dto/users/UserValidateRequestDTO"
import UserValidateResponseDTO from "../../domain/dto/users/UserValidateResponseDTO"
import UserRepository from "../../domain/repository/UserReporitory";
import TokenUtils from "../../utils/TokenUtil";
import UserTokenData from "../../domain/dto/UserTokenData";

export class UserApplication {
    constructor(private readonly userRepository: UserRepository) {}

    async findAll(): Promise<UserDAO[]> {
        return await this.userRepository.findAll();
    }

    async findOne(_id: number): Promise<UserDAO> {
        const item: Partial<UserDAO> = { id: _id};
        return await this.userRepository.findOne(item);
    }

    async validate(_userRequest: UserValidateRequestDTO): Promise<UserValidateResponseDTO> {
        const item: Partial<UserDAO> = { alias: _userRequest.alias, password: _userRequest.password};
        const user:UserDAO = await this.userRepository.findOne(item);
        const userTokenData:UserTokenData = { alias: user.alias, rights: [] }
        const token = TokenUtils.sing(userTokenData);
        return { user: user, token: token};
    }
}