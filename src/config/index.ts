export default class Config {
  private static _instance: Config;

  private static _port: number;
  private static _sqliteSource: string;
  private static _environment: string;
  private static _databaseType: string;
  private static _jwtSecret: string;
  private static _jwtExpirationMinutes: number;

  private constructor() {
    if (process.env.NODE_ENV === "test"){
      Config._port = 0; // Server listen in random port - https://stackoverflow.com/questions/54422849/jest-testing-multiple-test-file-port-3000-already-in-use/63293781#63293781
    }else{
      Config._port = Number(process.env.PORT) || 3001;
    }
    Config._sqliteSource = process.env.SQLITE_SOURCE ?? ":memory:";
    Config._environment = process.env.NODE_ENV || "development";
    Config._databaseType = process.env.DATABASE_TYPE ?? "sqlite";
    Config._jwtSecret = process.env.JWT_SECRET ?? "secret";
    Config._jwtExpirationMinutes =
      Number(process.env.JWT_EXPIRATION_MINUTES) || 480;
  }

  public get port(): number {
    return Config._port;
  }

  public get sqliteSource(): string {
    return Config._sqliteSource;
  }

  public get environment(): string {
    return Config._environment;
  }

  public get databaseType(): string {
    return Config._databaseType;
  }

  public get jwtSecret(): string {
    return Config._jwtSecret;
  }

  public get jwtExpirationMinutes(): number {
    return Config._jwtExpirationMinutes;
  }

  /**
   * Get the instance of Config
   */
  public static getInstance(): Config {
    if (typeof Config._instance === "undefined") {
      Config._instance = new Config();
    }

    return Config._instance;
  }

  public get toString(): string {
    const response = `port = ${Config._port}, \nsqliteSource = ${Config._sqliteSource}, \nenvironment = ${Config._environment}`;
    return response;
  }
}
