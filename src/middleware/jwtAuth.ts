import { NextFunction, Request, Response } from "express";

import ApiErrorNames from "../domain/dao/response/ApiErrorNames";
import { ApiErrorResponse } from "../domain/dao/response/ApiErrorResponse";
import HttpStatusCodes from "../domain/dao/response/HttpStatusCodes";
import TokenPayload from "../domain/dto/TokenPayload";
import { ApiResponseUtil } from "../utils/ApiResponseUtil";
import TokenUtils from "../utils/TokenUtil";


const responseUtil = new ApiResponseUtil();

export default (_req: Request, _res: Response, _next: NextFunction) => {
  try {
    const token = _req.header("x-auth-token");
    if (!token) {
      _res
        .status(HttpStatusCodes.FORBIDDEN)
        .send(
          responseUtil.customFailure(
            HttpStatusCodes.FORBIDDEN,
            HttpStatusCodes[HttpStatusCodes.FORBIDDEN],
            undefined,
            undefined,
            new ApiErrorResponse(
              ApiErrorNames.TOKEN_NOT_PRESENT,
              HttpStatusCodes.FORBIDDEN,
              "Token not present",
              true,
              undefined
            )
          )
        );
    } else {
      const decoded: TokenPayload = TokenUtils.verify(token) as TokenPayload;
      _req.user = decoded.data;
    }

    _next();
  } catch (e) {
    _res
      .status(HttpStatusCodes.UNAUTHORIZED)
      .send(
        responseUtil.customFailure(
          HttpStatusCodes.UNAUTHORIZED,
          HttpStatusCodes[HttpStatusCodes.UNAUTHORIZED],
          undefined,
          undefined,
          new ApiErrorResponse(
            ApiErrorNames.TOKEN_INVALID,
            HttpStatusCodes.UNAUTHORIZED,
            "Invalid token",
            true,
            undefined
          )
        )
      );
  }
};
