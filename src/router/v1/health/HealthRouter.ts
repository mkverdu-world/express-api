import express, { NextFunction, Request, Response } from "express";

export const healthRouter = express.Router();

healthRouter.get(
  "/",
  (_req: Request, _res: Response, _next: NextFunction): void => {
    const response = {
      status: "OK",
    };
    _res.status(200).json(response);
  }
);
