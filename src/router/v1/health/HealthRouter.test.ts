import request from "supertest";

import HttpStatusCodes from "../../../domain/dao/response/HttpStatusCodes";
import {app, server} from "../../../index";
import { apiRootV1 } from "../..";

describe("1. Health", () => {
    const endpointsFolder = "/health";

    test("1.1.I'm alive!", async () => {
        await request(app)
        .get(apiRootV1 + endpointsFolder + "/")
        .expect(HttpStatusCodes.OK)
        .then((response) => {
            const data = response.body;
            console.log(data);
            expect(data.status).toBeDefined();
            expect(data.status).toEqual(expect.any(String));
            expect(data.status).toEqual("OK");
        });
    });


    afterAll( async () => {
        server.close();
    });
});