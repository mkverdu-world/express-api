import request from "supertest";
import HttpStatusCodes from "../../../domain/dao/response/HttpStatusCodes";
import {app, server} from "../../../index";
import { apiRootV1 } from "../..";
import UserValidateRequestDTO from "../../../domain/dto/users/UserValidateRequestDTO";
import ApiErrorNames from "../../../domain/dao/response/ApiErrorNames";

describe("2. Users", () => {
    const endpointsFolder = "/users";
    let token = "";

    test("2.1.validate",async () => {
        const userValidateRequest: UserValidateRequestDTO = {
            alias: "test",
            password: "password"
        };
        await request(app)
        .post(apiRootV1 + endpointsFolder + "/validate")
        .send(userValidateRequest)
        .expect(HttpStatusCodes.OK)
        .then((response) => {
            const data = response.body.data;
            //console.log(data);
            expect(data).toEqual(expect.any(Object));
            expect(data.user).toBeDefined();
            expect(data.token).toBeDefined();
            token = data.token;
        });
    });

    test("2.2.findAll", async () => {
        await request(app)
        .get(apiRootV1 + endpointsFolder + "/")
        .set('x-auth-token', token)
        .expect(HttpStatusCodes.OK)
        .then((response) => {
            const data = response.body.data;
            //console.log(data);
            expect(data).toEqual(expect.any(Array));
        });
    });

    test("2.3.findOne",async () => {
        await request(app)
        .get(apiRootV1 + endpointsFolder + "/1")
        .set('x-auth-token', token)
        .expect(HttpStatusCodes.OK)
        .then((response) => {
            const data = response.body.data;
            //console.log(data);
            expect(data).toEqual(expect.any(Object));
            expect(data.id).toBeDefined();
            expect(data.alias).toBeDefined();
            expect(data.password).toBeDefined();
        });
    });

    test("2.4.findOne NOT_FOUND",async () => {
        await request(app)
        .get(apiRootV1 + endpointsFolder + "/9999")
        .set('x-auth-token', token)
        .expect(HttpStatusCodes.NOT_FOUND)
        .then((response) => {
            const body = response.body;
            //console.log(body);
            expect(body).toEqual(expect.any(Object));
            expect(body.error.code).toBeDefined();
            expect(body.error.code).toEqual(ApiErrorNames.USER_NOT_FOUND);
        });
    });

    test("2.5.findOne FORBIDDEN",async () => {
        await request(app)
        .get(apiRootV1 + endpointsFolder + "/1")
        .expect(HttpStatusCodes.FORBIDDEN)
        .then((response) => {
            const body = response.body;
            //console.log(body);
            expect(body).toEqual(expect.any(Object));
            expect(body.error.code).toBeDefined();
            expect(body.error.code).toEqual(ApiErrorNames.TOKEN_NOT_PRESENT);
        });
    });

    afterAll( async () => {
        server.close();
    });

});