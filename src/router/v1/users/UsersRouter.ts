import express from "express";

import jwtAuth from "../../../middleware/jwtAuth";
import { userController } from "../../../infrastructure/user/dependencies";

export const usersRouter = express.Router();

usersRouter.get("/", jwtAuth, userController.findAll.bind(userController));
usersRouter.get(
  "/:id([0-9]{1,9})", jwtAuth,
  userController.findOne.bind(userController)
);
usersRouter.post("/validate", userController.validate.bind(userController))
