import { NextFunction, Request, Response } from "express";

import { UserApplication } from "../../../application/user/UserApplication";
import { ApiResponseUtil } from "../../../utils/ApiResponseUtil";
import UserDAO from "../../../domain/dao/UserDAO";
import UserValidateResponseDTO from "../../../domain/dto/users/UserValidateResponseDTO"

export class UsersController {
    constructor(
        private readonly userApplication: UserApplication,
        private readonly responseUtils: ApiResponseUtil
    ) {}
    
    async findAll(_req: Request, _res: Response, _next: NextFunction) {
        try {
            const data = await this.userApplication.findAll();
            const response = this.responseUtils.success<UserDAO[]>(data, undefined);
            _res.status(200).json(response);
        }catch (e) {
            _next(e);
        }
    }

    async findOne(_req: Request, _res: Response, _next: NextFunction) {
        try {
          const id = Number(_req.params.id);
          const data = await this.userApplication.findOne(id);
          const response = this.responseUtils.success<UserDAO>(
            data,
            { id }
          );
          _res.status(200).json(response);
        } catch (e) {
          _next(e);
        }
    }

    async validate(_req: Request, _res: Response, _next: NextFunction){
        try{
            const data = await this.userApplication.validate(_req.body)
            const response = this.responseUtils.success<UserValidateResponseDTO>(data, undefined);
            _res.status(200).json(response);
        }catch(e){
            _next(e);
        }
    }
}
