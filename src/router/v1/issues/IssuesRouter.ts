import express from "express";

import jwtAuth from "../../../middleware/jwtAuth";
import { issueController } from "../../../infrastructure/issue/dependencies";

export const issuesRouter = express.Router();

issuesRouter.get("/", jwtAuth, issueController.findAll.bind(issueController));
issuesRouter.get(
  "/:id([0-9]{1,9})", jwtAuth,
  issueController.findOne.bind(issueController)
);
