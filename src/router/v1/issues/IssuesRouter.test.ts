import request from "supertest";
import HttpStatusCodes from "../../../domain/dao/response/HttpStatusCodes";
import {app, server} from "../../../index";
import { apiRootV1 } from "../..";
import UserValidateRequestDTO from "../../../domain/dto/users/UserValidateRequestDTO";
import Config from "../../../config";
import { SqliteUserRepository } from "../../../infrastructure/user/repository/SqliteUserRepository"
import UserRepository from "../../../domain/repository/UserReporitory";
import { UserApplication } from "../../../application/user/UserApplication";
import ApiErrorNames from "../../../domain/dao/response/ApiErrorNames";

describe("3. Issues", () => {
    const endpointsFolder = "/issues";
    let token = "";

    beforeAll(async () => {
        const userValidate: UserValidateRequestDTO = {
            alias: "test",
            password: "password"
        }

        const config = Config.getInstance();

        const repositories: Record<
        string,
        {
            new (): UserRepository;
        }
        > = {
        sqlite: SqliteUserRepository,
        };

        const getUserRepository = () => {
            if (repositories[config.databaseType]) {
            return new repositories[config.databaseType]();
            } else {
            throw Error(`Invalid Database type '${config.databaseType}'`);
            }
        };
        
        const userApplication = new UserApplication(getUserRepository());

        const data = await userApplication.validate(userValidate);
        token = data.token
    });

    test("3.1.findAll", async () => {
        await request(app)
        .get(apiRootV1 + endpointsFolder + "/")
        .set('x-auth-token', token)
        .expect(HttpStatusCodes.OK)
        .then((response) => {
            const data = response.body.data;
            //console.log(data);
            expect(data).toEqual(expect.any(Array));
        });
    });

    test("3.2.findOne",async () => {
        await request(app)
        .get(apiRootV1 + endpointsFolder + "/1")
        .set('x-auth-token', token)
        .expect(HttpStatusCodes.OK)
        .then((response) => {
            const data = response.body.data;
            //console.log(data);
            expect(data).toEqual(expect.any(Object));
            expect(data.id).toBeDefined();
            expect(data.title).toBeDefined();
            expect(data.description).toBeDefined();
        });
    });

    test("3.3.findOne NOT_FOUND",async () => {
        await request(app)
        .get(apiRootV1 + endpointsFolder + "/9999")
        .set('x-auth-token', token)
        .expect(HttpStatusCodes.NOT_FOUND)
        .then((response) => {
            const body = response.body;
            //console.log(body);
            expect(body).toEqual(expect.any(Object));
            expect(body.error.code).toBeDefined();
            expect(body.error.code).toEqual(ApiErrorNames.USER_NOT_FOUND);
        });
    });

    afterAll( async () => {
        server.close();
    });
});