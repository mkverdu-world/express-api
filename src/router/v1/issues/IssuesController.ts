import { NextFunction, Request, Response } from "express";

import { IssueApplication } from "../../../application/issues/IssueApplication";
import IssueDAO from "../../../domain/dao/IssueDAO";
import { ApiErrorResponse } from "../../../domain/dao/response/ApiErrorResponse";
import { ApiResponseUtil } from "../../../utils/ApiResponseUtil";

export class IssuesController {
  constructor(
    private readonly issueApplication: IssueApplication,
    private readonly responseUtils: ApiResponseUtil
  ) {}

  async findAll(_req: Request, _res: Response, _next: NextFunction) {
    try {
      const data = await this.issueApplication.findAll();
      const response = this.responseUtils.success<IssueDAO[]>(data, undefined);
      _res.status(200).json(response);
    } catch (e) {
      _next(e);
    }
  }

  async findOne(_req: Request, _res: Response, _next: NextFunction) {
    try {
      const id = Number(_req.params.id);
      const data = await this.issueApplication.findOne(id);
      const response = this.responseUtils.success<IssueDAO | ApiErrorResponse>(
        data,
        { id }
      );
      _res.status(200).json(response);
    } catch (e) {
      _next(e);
    }
  }
}
