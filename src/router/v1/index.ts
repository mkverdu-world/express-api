// import express, { Express, Router } from 'express'
import express from "express";

import { healthRouter } from "./health/HealthRouter";
import { issuesRouter } from "./issues/IssuesRouter";
import { usersRouter } from "./users/UsersRouter";

export const routerV1 = express.Router();

routerV1.use("/health", healthRouter);
routerV1.use("/issues", issuesRouter);
routerV1.use("/users", usersRouter);
