import { Express } from "express";

import { routerV1 } from "./v1";

export const apiRootV1 = "/api/v1/";

export const routerApi = (_app: Express): void => {
  _app.use(apiRootV1, routerV1);
};
