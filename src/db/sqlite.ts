import { AsyncDatabase } from "promised-sqlite3";

import Config from "../config";

const config = Config.getInstance();

export default class Sqlite {
  private static _instance: Sqlite;
  private static _db: AsyncDatabase;

  private static async run(): Promise<AsyncDatabase> {
    try {
      const db = await AsyncDatabase.open(config.sqliteSource);

      await db.run(
        'CREATE TABLE IF NOT EXISTS "issues" (title TEXT, description TEXT)'
      );

      // Adding some rows to table
      const insertIssue =
        "INSERT INTO issues (title, description) VALUES (?, ?)";
      await db.run(insertIssue, [
        "The total sum is not displayed",
        "report X does not show the totalized amount of products",
      ]);
      await db.run(insertIssue, [
        "The color does not correspond to the condition.",
        "when displaying the product, the background color does not correspond to the stock shortage",
      ]);

      // Adding some rows to table
      const insertUser = "INSERT INTO users (alias, password) VALUES (?, ?)";
      await db.run(
        'CREATE TABLE IF NOT EXISTS "users" (alias TEXT, password TEXT)'
      );

      await db.run(insertUser, [
        "test",
        "password",
      ]);

      // Close the database.
      //await db.close();

      return db;
    } catch (e) {
      return Promise.reject(e);
    }
  }

  public get db(): AsyncDatabase {
    return Sqlite._db;
  }

  public static async getInstance(): Promise<Sqlite> {
    if (typeof Sqlite._instance === "undefined") {
      Sqlite._instance = new Sqlite();
      Sqlite._db = await Sqlite.run();
    }

    return Sqlite._instance;
  }
}

// const db = new AsyncDatabase.open(config.sqliteSource, (err) => {
//   if (err != null) {
//     // Can't create database
//     console.error(err.message);
//     throw err;
//   } else {
//     console.log(`Created SQLite database on '${config.sqliteSource}'`);
//     db.run(
//       'CREATE TABLE IF NOT EXISTS "issues" (title TEXT, description TEXT)',
//       (err) => {
//         if (err != null) {
//           // Can't create ISSUES table
//           console.error(err.message);
//         } else {
//           // Adding some rows to table
//           const insert =
//             "INSERT INTO issues (title, description) VALUES (?, ?)";
//           db.run(insert, [
//             "No se muestra la suma del total",
//             "en el informe X no se muestra el totalizado del importe de productos",
//           ]);
//           db.run(insert, [
//             "El color no correcponde cno el estado",
//             "al mostrar el producto el color de fondo no corresponse con la fata de stock",
//           ]);
//         }
//       }
//     );
//   }
// });

// export default db;
