import express from "express";

import Config from "./config";
import ErrorHandler from "./handlers/ErrorHandler";
import { routerApi } from "./router";

process.on("uncaughtException", (_error: Error) => {
  ErrorHandler.appErrorHandler(_error);
  process.exit(1);
});

export const app = express();

// TODO: Crear Middleware para formatear las respuestas de la API
app.use(express.json());

routerApi(app);

// Error handlers
app.use(ErrorHandler.logErrors);
app.use(ErrorHandler.apiErrorHandler);
app.use(ErrorHandler.errorHandler);

const config = Config.getInstance();
export const server = app.listen(config.port, () => {
  if (config.environment === "development" || config.environment === "docker") {
    console.log(
      `Server. Initialized on http://localhost:${config.port}/api/v1/health\n`
    );
  }
  console.log("Environment VARS:");
  console.log(config.toString);
});
