import crypto from "crypto";
import { NextFunction, Request, Response } from "express";

import { ApiErrorResponse } from "../domain/dao/response/ApiErrorResponse";
import { ApiResponseUtil } from "../utils/ApiResponseUtil";

const apiResponseUtil = new ApiResponseUtil();

export default class ErrorHandler {
  static logErrors = (
    _err: any,
    _req: Request,
    _res: Response,
    _next: NextFunction
  ) => {
    console.log("logErrors");
    if (!_err.tagId) {
      _err.tagId = crypto.randomBytes(20).toString("hex");
    }
    console.error({ url: _req.url, user: _req.user });
    _next(_err);
  };

  static apiErrorHandler = (
    _err: any,
    _req: Request,
    _res: Response,
    _next: NextFunction
  ) => {
    console.log("apiErrorHandler");
    if (_err.isOperational) {
      if (_req.app.settings.env !== "development") {
        _err.stackTrace = undefined;
      }
      const response = apiResponseUtil.customFailure(
        _err.httpCode,
        _err.httpName,
        undefined,
        undefined,
        _err as ApiErrorResponse
      );
      _res.status(_err.httpCode).json(response);
    } else {
      _next(_err);
    }
  };

  static errorHandler = (_err: any, _req: any, _res: any, _next: any) => {
    console.log("errorHandler");
    let apiError;
    if (_err.sqlState) {
      // For SQL errors
      _err.name = "SQL Error";
      _err.isOperational = false;
    } else if (_err.name === "AxiosError") {
      _err.isOperational = false;
    } else {
      _err.name = "Error Not handled";
      _err.isOperational = false;
      _err.stackTrace = _err.stack;
    }
    const response = apiResponseUtil.internalError(
      undefined,
      _err as ApiErrorResponse,
      undefined
    );
    _res.status(response.statusCode).json(response);
  };

  static appErrorHandler = (_err: Error) => {
    console.log("appErrorHandler");
    console.error(JSON.stringify(_err, Object.getOwnPropertyNames(_err)));
  };
}
