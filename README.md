# Basic project using hexagonal architecture and good practices

## Description
Project to take an starting template for a API with hexagonal architecture and good practices code

## Features
- Automated checking of your source code for programmatic and stylistic errors using a _lint tool_
- Securized by JWT token
- Standardized response object providing detailed information with fields like unique _tag ID_ for error, _success_ flag, _isOperational_ flag, _stackTrace_, and _custom error codes_ among others.
- Providing support to load environment variables from a _.env_ file into the _process.env_ object
- _Self testing_ to be sure the code works properly
- Available to execute in a _Docker container_ locally

## Powered By [![poweredBy](https://gitlab.com/uploads/-/system/user/avatar/10698594/avatar.png?width=32)](https://gitlab.com/MkVerdu)

## Installation

API requires [Node.js](https://nodejs.org/) v18.16.0 to ensure smooth run.

Install the dependencies and devDependencies and start the server.

```bash
npm i
npm run start:dev
```

For production environments...

```bash
npm install --production
npm run build
npm run start
```

NPM commands available to run with `npm run <command>`
- build
- reinstall
- rebuild
- rebuildsqlite
- checknodeversion
- start
- start:dev
- start:dev-for-docker
- test
- lint
- lint:fix
- tsc
- docker:start
- docker:stop

## Tips
Executedesired test using `npx jest --config=jest.config.json --detectOpenHandles -t '<test_name>'`
```bash
npx jest --config=jest.config.json --detectOpenHandles -t '3.1.someTests - OK'
```


## TODOs
- Keep finding some good ideas for the architecture. [Source](https://github.com/AlbertHernandez/repository-pattern-typescript-example/tree/main)

## Sources
  - Check the node version necessary to run it. [Source](https://medium.com/@faith__ngetich/locking-down-a-project-to-a-specific-node-version-using-nvmrc-and-or-engines-e5fd19144245)
 - Database initialization. [Source](https://developerhowto.com/2018/12/29/build-a-rest-api-with-node-js-and-express-js/)
 - Good practice using Records to select the database to manage calling a function. [Source](https://itnext.io/use-typescript-record-types-for-better-code-ce1768c6cb53)
 - Create a generic repository for all entities. [Source](https://dev.to/fyapy/repository-pattern-with-typescript-and-nodejs-25da) [Source](https://medium.com/@erickwendel/generic-repository-with-typescript-and-node-js-731c10a1b98e)
- Managing errors not covered in the application. [Source](https://github.com/goldbergyoni/nodebestpractices/blob/master/sections/errorhandling/shuttingtheprocess.md)
 - Data injection in the Request. [Source](https://dev.to/kwabenberko/extend-express-s-request-object-with-typescript-declaration-merging-1nn5) [Source](https://blog.logrocket.com/extend-express-request-object-typescript/)
 - How to create a random key to use as JWT_SECRET with node. [Source](https://github.com/dwyl/learn-json-web-tokens)
   `node -e "console.log(require('crypto').randomBytes(32).toString('hex'));"`
 - Applying authentication with JWT. [Source](https://dev.to/cyberwolves/node-js-api-authentication-with-jwt-json-web-token-auth-middleware-ggm)


 ## Thanks to all people who share her knowledge and ideas to debate and achieve betters ways to solve any problem