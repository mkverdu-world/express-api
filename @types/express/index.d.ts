import UserTokenData from "../../src/domain/dto/UserTokenData";

declare module "express-serve-static-core" {
  export interface Request {
    user?: UserTokenData;
  }
}
